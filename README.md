# Todoist Sync API TypeScript (todoist-sync-api-ts)

## Warning! This library is a work in progress and not at all feature complete

A simple wrapper for the official [Todoist Sync API](https://developer.todoist.com/sync/v8/) written in TypeScript.
