export type TodoistResourceType = 'all' | 'projects' | 'notes' | 'labels';

export interface TodoistProject {
  /** The id of the project. */
  id: number;
  /**
   * The legacy id of the project.
   * (only shown for objects created before 1 April 2017)
   */
  legacy_id: number;
  /** The name of the project. */
  name: string;

  is_archived: number;
  color: number;
  shared: boolean;
  inbox_project: boolean;
  collapsed: number;
  child_order: number;
  is_deleted: number;
  parent_id: number | null;
}

export interface TodoistDueObject {
  /** @example 2016-08-01T13:19:45Z */
  date: string;
  timezone: string | null;
  is_recurring: boolean;
  /** @example 'tomorrow at 10:00' */
  string: string;
  /** @example 'en' */
  lang: string;
}

export type TodoistTaskId = string;

interface TodoistTask {
  id: TodoistTaskId;
  collapsed: number;
  /** @example 2016-08-01T13:19:45Z */
  date_added: string;
  child_order: number;
  parent_id: number | null;
  all_day: boolean;
  day_order: number;
  added_by_uid: number;
  assigned_by_uid: number;
  responsible_uid: any | null;
  sync_id: any | null;
  checked: number;
  user_id: number;
  labels: any[];
  is_deleted: number;
  due: TodoistDueObject | null;
  project_id: number;
  in_history: number;
  content: string;
  priority: number;
}

export interface TodoistResponse {
  /** Whether the request was a full sync. */
  full_sync: boolean;
  /** Temporary token used for incremental syncs. */
  sync_token: string;
  /**
   * Maps temp IDs passed when running sync commands to their actual Todoist IDs on the server.
   */
  temp_id_mapping: Record<string, number>;
  /** Used to check if a command with a certain temp ID succeeded or in case of error what exactly was the problem. */
  sync_status?: Record<string, 'ok' | string>

  projects?: TodoistProject[];
  /** Tasks */
  items?: TodoistTask[];
}

export interface TodoistProjectsResponse extends TodoistResponse {
  projects: TodoistProject[];
}
