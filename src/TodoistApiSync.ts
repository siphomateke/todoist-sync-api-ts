import Axios, { AxiosInstance } from 'axios';
import rateLimit from 'axios-rate-limit';

import {
  TodoistResourceType,
  TodoistResponse,
  TodoistProjectsResponse,
  TodoistDueObject,
  TodoistTaskId,
} from './types';

interface SyncCommand {
  type: string;
  temp_id?: string;
  uuid?: string;
  args?: Record<string, unknown>;
}

interface ProjectAddCommand extends SyncCommand {
  type: 'project_add';
  args: {
    name: string;
  };
}

interface AddTaskCommand extends SyncCommand {
  type: 'item_add';
  args: {
    project_id: string | number;
    content?: string;
  };
}

interface UpdateTaskCommand extends SyncCommand {
  type: 'item_update';
  args: {
    id: TodoistTaskId;
    content?: string;
    due?: Partial<TodoistDueObject>;
  };
}

interface CompleteTaskCommand extends SyncCommand {
  type: 'item_complete';
  args: {
    ids: TodoistTaskId[];
  };
}

interface DeleteTaskCommand extends SyncCommand {
  type: 'item_delete';
  args: {
    ids: TodoistTaskId[];
  };
}

type SyncCommands = ProjectAddCommand | AddTaskCommand | UpdateTaskCommand | CompleteTaskCommand | DeleteTaskCommand;

interface RequestOptions {
  data?: Record<string, unknown>;
  resourceTypes?: TodoistResourceType[];
  excludeResourceTypes?: TodoistResourceType[];
  commands?: SyncCommand[];
  syncToken?: string;
}

export default class TodoistApiSync {
  private token: string;

  private axiosInstance: AxiosInstance;

  constructor(token: string) {
    this.token = token;
    const oneMinute = 1 * 60 * 1000;
    this.axiosInstance = rateLimit(Axios.create({
      headers: { 'Content-Type': 'application/json' },
    }), {
      // TODO: Implement batching limit of 100 commands
      // See https://developer.todoist.com/sync/v8/#batching-commands
      maxRequests: 50,
      perMilliseconds: oneMinute,
    });
  }

  async request<T extends TodoistResponse>({
    data,
    resourceTypes,
    excludeResourceTypes,
    commands,
    syncToken,
  }: RequestOptions): Promise<T> {
    const todoistResourceTypes: string[] = resourceTypes || [];
    if (excludeResourceTypes) {
      // To exclude resource types, add a hyphen before the name of the resource.
      // See https://developer.todoist.com/sync/v8/#excluding-resource-types
      todoistResourceTypes.push(...excludeResourceTypes.map((t) => `-${t}`));
    }
    const response = await this.axiosInstance.post<T>('https://api.todoist.com/sync/v8/sync', {
      token: this.token,
      // If a sync token isn't provided, do a full sync by passing '*'.
      sync_token: syncToken || '*',
      resource_types: todoistResourceTypes,
      commands,
      ...data,
    });
    return response.data;
  }

  getAllProjects(): Promise<TodoistProjectsResponse> {
    return this.request({ resourceTypes: ['projects'] });
  }

  runCommands(commands: SyncCommands[]): Promise<TodoistResponse> {
    return this.request({ commands });
  }
}
